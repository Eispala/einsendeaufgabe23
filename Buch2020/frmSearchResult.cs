using System.Windows.Forms;

namespace Buch2020
{
    public partial class frmSearchResult : Form
    {
        public frmSearchResult(System.Data.DataTable table)
        {
            InitializeComponent();
            gridResults.DataSource = table;
        }
    }
}