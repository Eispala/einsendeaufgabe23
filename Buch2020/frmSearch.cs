using System;
using System.Windows.Forms;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Buch2020
{
    public partial class frmSearch : Form
    {
        frmSearchResult frmSearchResult;
        DataTable resultTable = new DataTable();

        public frmSearch()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var x = ConfigurationManager.ConnectionStrings["Buch2020.Properties.Settings.buch2020ConnectionString"]
                .ConnectionString;

            using (var connection = new SqlConnection {ConnectionString = x})
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand($"select * from kunde where kName like '%{tbSearch.Text}%';",
                    connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        resultTable.Clear();
                        resultTable.Load(reader);
                    }
                }


                connection.Close();
            }

            if (frmSearchResult == null)
            {
                frmSearchResult = new frmSearchResult(resultTable);
            }

            frmSearchResult.ShowDialog();
        }
    }
}