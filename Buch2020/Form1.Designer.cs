﻿namespace Buch2020
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonEinzelansicht = new System.Windows.Forms.Button();
            this.buttonListenansicht = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonMedienliste = new System.Windows.Forms.Button();
            this.buttonMEinzelansicht = new System.Windows.Forms.Button();
            this.buttonMListenansicht = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonReservierungAnlegen = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonAusleihe = new System.Windows.Forms.Button();
            this.buttonRueckgabe = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonEinzelansicht);
            this.groupBox1.Controls.Add(this.buttonListenansicht);
            this.groupBox1.Location = new System.Drawing.Point(10, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(233, 115);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kunden";
            // 
            // buttonEinzelansicht
            // 
            this.buttonEinzelansicht.Location = new System.Drawing.Point(64, 55);
            this.buttonEinzelansicht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonEinzelansicht.Name = "buttonEinzelansicht";
            this.buttonEinzelansicht.Size = new System.Drawing.Size(104, 27);
            this.buttonEinzelansicht.TabIndex = 1;
            this.buttonEinzelansicht.Text = "Einzelansicht";
            this.buttonEinzelansicht.UseVisualStyleBackColor = true;
            this.buttonEinzelansicht.Click += new System.EventHandler(this.buttonEinzelansicht_Click);
            // 
            // buttonListenansicht
            // 
            this.buttonListenansicht.Location = new System.Drawing.Point(64, 22);
            this.buttonListenansicht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonListenansicht.Name = "buttonListenansicht";
            this.buttonListenansicht.Size = new System.Drawing.Size(104, 27);
            this.buttonListenansicht.TabIndex = 0;
            this.buttonListenansicht.Text = "Listenansicht";
            this.buttonListenansicht.UseVisualStyleBackColor = true;
            this.buttonListenansicht.Click += new System.EventHandler(this.buttonListenansicht_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonMedienliste);
            this.groupBox2.Controls.Add(this.buttonMEinzelansicht);
            this.groupBox2.Controls.Add(this.buttonMListenansicht);
            this.groupBox2.Location = new System.Drawing.Point(268, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(233, 148);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Medien";
            // 
            // buttonMedienliste
            // 
            this.buttonMedienliste.Location = new System.Drawing.Point(68, 114);
            this.buttonMedienliste.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonMedienliste.Name = "buttonMedienliste";
            this.buttonMedienliste.Size = new System.Drawing.Size(104, 27);
            this.buttonMedienliste.TabIndex = 4;
            this.buttonMedienliste.Text = "Medienliste";
            this.buttonMedienliste.UseVisualStyleBackColor = true;
            this.buttonMedienliste.Click += new System.EventHandler(this.buttonMedienliste_Click);
            // 
            // buttonMEinzelansicht
            // 
            this.buttonMEinzelansicht.Location = new System.Drawing.Point(68, 55);
            this.buttonMEinzelansicht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonMEinzelansicht.Name = "buttonMEinzelansicht";
            this.buttonMEinzelansicht.Size = new System.Drawing.Size(104, 27);
            this.buttonMEinzelansicht.TabIndex = 3;
            this.buttonMEinzelansicht.Text = "Einzelansicht";
            this.buttonMEinzelansicht.UseVisualStyleBackColor = true;
            this.buttonMEinzelansicht.Click += new System.EventHandler(this.buttonMEinzelansicht_Click);
            // 
            // buttonMListenansicht
            // 
            this.buttonMListenansicht.Location = new System.Drawing.Point(68, 22);
            this.buttonMListenansicht.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonMListenansicht.Name = "buttonMListenansicht";
            this.buttonMListenansicht.Size = new System.Drawing.Size(104, 27);
            this.buttonMListenansicht.TabIndex = 2;
            this.buttonMListenansicht.Text = "Listenansicht";
            this.buttonMListenansicht.UseVisualStyleBackColor = true;
            this.buttonMListenansicht.Click += new System.EventHandler(this.buttonMListenansicht_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonReservierungAnlegen);
            this.groupBox4.Location = new System.Drawing.Point(265, 195);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Size = new System.Drawing.Size(233, 157);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Reservierungen";
            // 
            // buttonReservierungAnlegen
            // 
            this.buttonReservierungAnlegen.Location = new System.Drawing.Point(64, 22);
            this.buttonReservierungAnlegen.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonReservierungAnlegen.Name = "buttonReservierungAnlegen";
            this.buttonReservierungAnlegen.Size = new System.Drawing.Size(104, 27);
            this.buttonReservierungAnlegen.TabIndex = 0;
            this.buttonReservierungAnlegen.Text = "Anlegen";
            this.buttonReservierungAnlegen.UseVisualStyleBackColor = true;
            this.buttonReservierungAnlegen.Click += new System.EventHandler(this.buttonReservierungAnlegen_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonAusleihe);
            this.groupBox3.Controls.Add(this.buttonRueckgabe);
            this.groupBox3.Location = new System.Drawing.Point(7, 195);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(233, 157);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Verträge";
            // 
            // buttonAusleihe
            // 
            this.buttonAusleihe.Location = new System.Drawing.Point(64, 22);
            this.buttonAusleihe.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonAusleihe.Name = "buttonAusleihe";
            this.buttonAusleihe.Size = new System.Drawing.Size(104, 27);
            this.buttonAusleihe.TabIndex = 0;
            this.buttonAusleihe.Text = "Ausleihe";
            this.buttonAusleihe.UseVisualStyleBackColor = true;
            this.buttonAusleihe.Click += new System.EventHandler(this.buttonAusleihe_Click);
            // 
            // buttonRueckgabe
            // 
            this.buttonRueckgabe.Location = new System.Drawing.Point(64, 55);
            this.buttonRueckgabe.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonRueckgabe.Name = "buttonRueckgabe";
            this.buttonRueckgabe.Size = new System.Drawing.Size(104, 27);
            this.buttonRueckgabe.TabIndex = 0;
            this.buttonRueckgabe.Text = "Rückgabe";
            this.buttonRueckgabe.UseVisualStyleBackColor = true;
            this.buttonRueckgabe.Click += new System.EventHandler(this.buttonRueckgabe_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSearch);
            this.groupBox5.Location = new System.Drawing.Point(10, 358);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox5.Size = new System.Drawing.Size(233, 83);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Verträge";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(61, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(104, 27);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Kundensuche";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 475);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "Buch2020";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonListenansicht;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonEinzelansicht;
        private System.Windows.Forms.Button buttonMEinzelansicht;
        private System.Windows.Forms.Button buttonMListenansicht;
        private System.Windows.Forms.Button buttonMedienliste;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonReservierungAnlegen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonAusleihe;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button buttonRueckgabe;
    }
}

